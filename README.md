# [Setup, Getting Started, And More Here](../../wiki/Home)

# About the Author(s), Contributors
|      |      |
|---------|---------|
| ![Cnris Cilino](https://media-exp1.licdn.com/dms/image/C4E03AQEdGcEOIf3fJg/profile-displayphoto-shrink_200_200/0?e=1605744000&v=beta&t=tcHfVvmnJayNLa8aZznp0BbM4Jyq_87IGJbA4L903ko)| I ([Chris Cilino](https://bit.ly/ChrisCilino_LinkedIn)) believe in good software engineering and healthy team builiding. I founded [PetranWay](https://www.petranway.com) to help software organizations grow thier software expertise, nurture team culture, and build infrastructure so that they produce quality products that meet business objectives. For more information check out [PetranWay](https://www.petranway.com).|
| ![Navin Subramani](https://media-exp1.licdn.com/dms/image/C4E03AQF-UmOB49pEFQ/profile-displayphoto-shrink_200_200/0?e=1607558400&v=beta&t=GZAMo2dAjXNs_oWUFHd2N60xVT_-a4IYE-4M4xy4izg)| [Navin Subramni](https://www.linkedin.com/in/navin-subramani-92b18378/)|


Please feel free to check out 

* [This Repository's Wiki](../../wiki/Home)
* [PetranWay](https://www.petranway.com)
* [My Online Presentations](http://bit.ly/ChrisCilino_Presentations)
* [My Free and Open Source Code](http://bit.ly/ChrisCilino_CSuite)
* [My LinkedIn Profile](http://bit.ly/ChrisCilino_LinkedIn)


# License
All software in this repository is licensed under the MIT license found in [..\Build\License\License.rtf](../master/Build/License/License.rtf). 